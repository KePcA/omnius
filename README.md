# Engineering Challenge for Python Developer
This is a repository containing a solution of the engineering code challenge
for Python Developer job position at Omnius. It consists of two challenges
to evaluate the coding skill of the candidate.

## Build Instructions
Simply clone the repository to your local machine and all the
necessary files will be there.
Beside the **Python** you should also have a **JSON library** installed at 
your system for exporting the data from the JSON files.

Repository consists of the following files:

* **challenge_1.py**: solution to the first challenge containing three functions.
```
def problem_1(documents)
def problem_2(status, documents)
def problem_3(file_name, documents)
```
* **challenge_2.py**: solution to the second challenge containing function to flatten a tree to a dictionary.
```
def flatten(data, sep="__")
```
* **main_1.py**: executing individual functions of the _challenge_1.py_.
* **main_2.py**: executing individual functions of the _challenge_2.py_.

Repository consists of the following resources:

* api-response.json
* sample-1.json
* sample-2.json
* sample-3.json
* sample-4.json

First resource is used for testing the functions of the first challenge.
The rest four are used for testing the function of the second
challenge.
Resources can also be found at the following links:

* https://raw.githubusercontent.com/omni-us/coding-challengesresources/master/python-developer/api-response.json 
* https://raw.githubusercontent.com/omni-us/coding-challengesresources/master/python-developer/sample-1.json
* https://raw.githubusercontent.com/omni-us/coding-challengesresources/master/python-developer/sample-2.json
* https://raw.githubusercontent.com/omni-us/coding-challengesresources/master/python-developer/sample-3.json 

## Run instructions
Two main files are used to run the implemented functions
of this challenge.

### main_1.py:
Executing individual functions of the _challenge_1.py_. There are two 
helper methods:

* **export_documents(json_file)**: extracting documents from the JSON file.
* **print_list(list)**: prints each item in a list.

We load the data from the _api-response.json_.
```
documents = export_documents('api-response.json')
```
To use another resource just replace the parameter of the above 
function.

Then we just execute all the implemented functions and print the
results. 

Under the _PROBLEM_2_ section, we can change the value of the **status**
variable to:

* REOPENED
* VALIDATED
* DONE
* TERMINATED
* PROCESSED
* FAILED
* MIGRATED
* CLOSED
* QUEUED
* PROCESSING

Under the _PROBLEM_3_ section, we can change the value of the **file_name**
variable to filter other documents.

### main_2.py:
Executing individual functions of the _challenge_2.py_. There are two 
helper methods:

* **export_data(json_file)**: extracting data from the JSON file.
* **print_dictionary(dictionary)**: prints each item in a dictionary.

We load the data from the JSON files. 
```
data_1 = export_data('sample-1.json')
data_2 = export_data('sample-2.json')
data_3 = export_data('sample-3.json')
data_4 = export_data('sample-4.json')
```

We flatten trees and convert them into the dictionary.
```
tree_dict_1 = challenge_2.flatten(data_1)
tree_dict_2 = challenge_2.flatten(data_2)
tree_dict_3 = challenge_2.flatten(data_3)
tree_dict_4 = challenge_2.flatten(data_4)
```

In the end, we print the results. We can easily add more resources.