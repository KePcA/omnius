

def biggest_possible_number(numbers):
    #Get list of highest numbers by sorting their digits.
    numbers = [int("".join(sorted(str(number), reverse=True))) for number in numbers]
    extended_numbers = []
    result = ""
    #Number of digits in the largest number(+1).
    len_highest = len(str(max(numbers))) + 1
    #Create extended numbers and add it to the list.
    for number in numbers:
        temp = str(number) * len_highest
        extended_numbers.append((temp[:len_highest:], number))
    #Sort by the extended numbers.
    extended_numbers.sort(reverse=True)
    #Concatinate the sorted numbers.
    for extended_number in extended_numbers:
        result += str(extended_number[1])
    return result


numbers = [13, 78, 56, 1, 5, 54, 60, 15, 121, 60]
print(biggest_possible_number(numbers))

