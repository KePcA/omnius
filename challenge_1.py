
'''
Captures the status vise distribution of the documents. From the list of documents
it creates a dictionary whose keys are different statuses and values are list of
documents which belong to these statuses. To identify how many documents belong to
each status we just return the length of the list that belongs to particular status.
'''
def problem_1(documents):
    status_dict = dict()
    for document in documents:
        #The status key already exsist. We append the document to that list.
        if document["status"] in status_dict:
            status_dict[document["status"]].append(document)
        #First document of that status. We define new key and add first document of that status to a list.
        else:
            status_dict[document["status"]] = [document]
    return status_dict


'''
Accepts status as an argument and returns all the documents 
details(without status attribute) belonging to that status.
'''
def problem_2(status, documents):
    status_dict = problem_1(documents)
    if status in status_dict:
        # Return the list of documents(without status attribute) from the dictionary with status as a key.
        return [{key:val for key, val in document.items() if key != "status"} for document in status_dict[status]]
    else:
        return []


'''
Accepts file_name as an argument and returns all the documents 
details(without file_name attribute) belonging to that file_name.
'''
def problem_3(file_name, documents):
    #Filters the documents which belong to that file_name and returns all the details(without file_name attribute)
    return [{key:val for key, val in document.items() if key != "file_name"} for document in documents if document["file_name"] == file_name]
