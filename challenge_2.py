
'''
Flattens a tree and converts it into a dictionary by
doing breadth-first search traversal of the tree.
'''
def flatten(data, sep="__"):
    tree_dict = dict()
    #First(and only) key is the root key.
    root_key = list(data.keys())[0]
    #Set up a queue with its first element - a tuple with root key and the dictionary representing the whole tree.
    trees = [(root_key, data[root_key])]
    #While there are still nodes to explore.
    while trees:
        #Pop the first element of the queue and unpack its values.
        parent_key, tree = trees.pop(0)
        #Create and initialize a new element in the dictionary.
        tree_dict[parent_key] = []
        #There are still some subtrees we have to explore.
        if isinstance(tree, dict):
            for key, subtree in tree.items():
                subtree_key = parent_key + sep + key
                tree_dict[parent_key].append(subtree_key)
                #Add the tuple to the end of the queue to be explored later.
                trees.append((subtree_key, subtree))
        '''
        else:
            tree_dict[parent_key] = tree
        '''
    return tree_dict


'''
Expands a dictionary representing flatten tree to the whole tree.
'''
def expand(tree_dict, key, sep="__"):
    #if isinstance(tree_dict[key], list):
    if tree_dict[key]:
        return {subkey.split(sep)[-1]: expand(tree_dict, subkey) for subkey in tree_dict[key]}
    else:
        #return tree_dict[key]
        return ""