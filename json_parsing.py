import json

file = open('json_file.json', 'r')

string_data = file.read()
print(type(string_data))
print(string_data)
print()

json_data = json.loads(string_data)
print(type(json_data))
print(json_data)
print()

string_data_again = json.dumps(json_data)
print(type(string_data_again))
print(string_data_again)
print()

json_data_again = json.loads(string_data_again)
print(type(json_data_again))
print(json_data_again)
print()

