import json
import challenge_1

'''
Helper function for extracting list of documents from the json file.
'''
def export_documents(json_file):
    f = open(json_file, 'r')
    data = json.load(f)
    documents = data["payload"]["items"]
    return documents

'''
Helper function to print each item in a list.
'''
def print_list(list):
    print("Size: ", len(list))
    for item in list:
        print(item)
    print()


documents = export_documents('api-response.json')

print("--------------------PROBLEM_1--------------------")
status_dict = challenge_1.problem_1(documents)
for key, value in status_dict.items():
    print(key + ":(" + str(len(value)) + ")", value)
print()

print("--------------------PROBLEM_2--------------------")
status = "TERMINATED"
print("Status:", status)
status_documents = challenge_1.problem_2(status, documents)
print_list(status_documents)

print("--------------------PROBLEM_3--------------------")
file_name = "55.pdf"
print("File name:", file_name)
file_name_documents = challenge_1.problem_3(file_name, documents)
print_list(file_name_documents)