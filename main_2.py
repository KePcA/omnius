import json
import challenge_2

'''
Helper function for extracting data representing tree from the json file.
'''
def export_data(json_file):
    f = open(json_file, 'r')
    data = json.load(f)
    return data


'''
Helper function to print each item in a dictionary.
'''
def print_dictionary(dictionary):
    print("Size: ", len(dictionary))
    for key, value in dictionary.items():
        print(key + ":", value)
    print()


data_1 = export_data('sample-1.json')
data_2 = export_data('sample-2.json')
data_3 = export_data('sample-3.json')
data_4 = export_data('sample-4.json')

tree_dict_1 = challenge_2.flatten(data_1)
tree_dict_2 = challenge_2.flatten(data_2)
tree_dict_3 = challenge_2.flatten(data_3)
tree_dict_4 = challenge_2.flatten(data_4)

print("--------------------SAMPLE_1--------------------")
print_dictionary(tree_dict_1)

print("--------------------SAMPLE_2--------------------")
print_dictionary(tree_dict_2)

print("--------------------SAMPLE_3--------------------")
print_dictionary(tree_dict_3)

print("--------------------SAMPLE_4--------------------")
print_dictionary(tree_dict_4)


print("--------------------EXPANDING TREE--------------------")
root_key = list(tree_dict_4.keys())[0]
expanded_tree = challenge_2.expand(tree_dict_4, root_key)
expanded_tree = {root_key: expanded_tree}
print(expanded_tree)
print(data_4)