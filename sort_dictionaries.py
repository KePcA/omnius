
data = [
    {
        "id": 1,
        "name": "Elizabeth",
        "salary": 65000
    },
    {
        "id": 2,
        "name": "Darwin",
        "salary": 60000
    },
    {
        "id": 3,
        "name": "Marco",
        "salary": 70000
    },
    {
        "id": 4,
        "name": "Dave",
        "salary": 60000
    },
    {
        "id": 5,
        "name": "Austin",
        "salary": 55000
    },
    {
        "id": 6,
        "name": "John",
        "salary": 70000
    },
    {
        "id": 7,
        "name": "Larissa",
        "salary": 60000
    }
]

print(data)
#Sorting by the key: by salary descending and by name ascending
data_sorted = sorted(data, key=lambda d: (-d['salary'], d['name']))
print(data_sorted)

